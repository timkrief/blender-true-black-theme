# Blender True Black Theme

This is a true black theme made for blender. This perfect for OLED screens. 

It's a first draft, it might be rough around the edges. So, if you encounter any issue, please open a ticket or even better, correct it and contribute to this repo.

I used a green / lime accent color, (if you edit the theme to change the accent color, tell me so that I can add it here as an option if it's pretty :wink:).

![Screenshot](true_black_theme_screenshot.png "Screenshot")

# Install

- Go to:

    Edit :arrow_right: :gear: Preferences… :arrow_right: Themes :arrow_right: :inbox_tray: Install

 - Then choose the file [true_black.xml](true_black.xml)

# Notes

Made for the great [Blender](https://www.blender.org/):

> Blender is the free and open source 3D creation suite. It supports the entirety of the 3D pipeline—modeling, rigging, animation, simulation, rendering, compositing and motion tracking, video editing and 2D animation pipeline. 

Tested on Blender v2.80